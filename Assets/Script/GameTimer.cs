﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameTimer : MonoBehaviour {

    private float totalTime;
    public int minute = 1;
    public float seconds = 30;
    private float oldSeconds;
    public float reloadSeconds = 5;
    public UnityEngine.UI.Text timerLabel;
    public GameObject timeupLabel;
    public bool is_clear { get; set; }
    public float hurryupTime = 20f;
    public float bgmPitch = 1.5f;
    private AudioSource bgm;
    
    void Start()
    {
        totalTime = minute * 60 + seconds;
        oldSeconds = 0f;
    }

    void Update(){
        // When you cleared, timer stop
        if (is_clear){
            return;
        }
        // if time is less than 0s, timeupLabel show.
        if (totalTime <= 0f){
            timeupLabel.SetActive(true);
            totalTime -= Time.deltaTime;
            // after reloadsSeconds, reload scene
            if (totalTime <= -reloadSeconds){
                // Get scene index
                int sceneIndex = SceneManager.GetActiveScene().buildIndex;
                // Reload scene
                SceneManager.LoadScene(sceneIndex);
            }
            return;
        }

        // calculate totalTime
        totalTime = minute * 60 + seconds;
        totalTime -= Time.deltaTime;

        // set time again
        minute = (int)totalTime / 60;
        seconds = totalTime - minute * 60;
        // If time remain less than hurryupTIme, BGM pitch change.
        if (totalTime < hurryupTime){
            bgm.pitch = bgmPitch;
        }

        // rewrite timerLabel
        if ((int)seconds != (int)oldSeconds){
            timerLabel.text = minute.ToString("00") + ":" + ((int)seconds).ToString("00");
        }
        oldSeconds = seconds;
    }
}
