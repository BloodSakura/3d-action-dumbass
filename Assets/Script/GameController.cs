﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameController : MonoBehaviour {

    public UnityEngine.UI.Text scoreLabel;
    public GameObject winnerLabelObject;
    public GameObject player2;
    public GameObject player2_special;
    public float timeOut = 30; //[s]
    private float timeElapsed;
    public UnityEvent onGameClear;
    public bool is_special { get; set; }

    void quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_STANDALONE
        UNityEngine.Application.Quit();
#endif
    }

    // Update is called once per frame
    void Update(){
        if (Input.GetKey(KeyCode.Escape))
        {
            quit();
        }
        int count = GameObject.FindGameObjectsWithTag("item").Length;
        scoreLabel.text = count.ToString();

        if (count == 0){
            // prougram when game player wins
            winnerLabelObject.SetActive (true);
            onGameClear.Invoke();
        }
        if (is_special)
        {
            if (timeElapsed == 0)
            {
                changePlayer(player2, player2_special);
            }
            // when is-special is true, add timeElapse
            timeElapsed += Time.deltaTime;
            if (timeElapsed >= timeOut)
            {
                changePlayer(player2_special, player2);
                is_special = false;
                timeElapsed = 0;
            }
        }
        if (is_special)
        {
            changePlayer(player2, player2_special);
        }
        void changePlayer (GameObject deactobj, GameObject actobj)
        {
            deactobj.SetActive(false);
            actobj.SetActive(true);
            actobj.transform.position = deactobj.transform.position;
            actobj.transform.rotation = deactobj.transform.rotation;
        }
    }
}
