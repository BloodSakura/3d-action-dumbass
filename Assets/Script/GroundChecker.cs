﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GroundChecker : MonoBehaviour {
    public UnityEvent onEnterGround;
    public UnityEvent onExitGround;
    private int enterNum = 0;
    /* Called this method when GroundChecker touches a game object with another Rigidbody or Collidor component */
    private void OnTriggerEnter(Collider collision){
        enterNum++;
        onEnterGround.Invoke();
    }
    /* Called this method when GroundChecker loses touch with other Rigidbody or other game object with collidor components */
    private void OnTriggerExit(Collider Collision)
    {
        enterNum--;
        if (enterNum <= 0)
        {
            onExitGround.Invoke();
        }
    }
    
}


