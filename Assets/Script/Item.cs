﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {
    public AudioClip soundGetItem;
    // The call-back of contact trigger
    void OnTriggerEnter (Collider hit)
    {
        // Is the contact target "Player" tag?
        if (hit.CompareTag("Player")){
            AudioSource.PlayClipAtPoint(soundGetItem, transform.position);
            // Destroy the Gameobject with this component
            Destroy(gameObject);
        }
    }
}
