﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        Invoke("Delay", 3.0f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void Delay()
    {
        SceneManager.LoadScene("Scene01");
    }
}
