﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class SpecialItem : Item
{
    public UnityEvent onSpecialItem;
    // The call-back of contact trigger
    private void OnTriggerEnter(Collider hit) 
    {
        // Is the contact target "Player" tag:
        if (hit.CompareTag("Player"))
        {
            AudioSource.PlayClipAtPoint(soundGetItem, transform.position);
            // Do some processing
            Destroy(gameObject);
            //occur event
            onSpecialItem.Invoke();
        }
    }
}
